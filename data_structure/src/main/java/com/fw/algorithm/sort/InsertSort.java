package com.fw.algorithm.sort;

import java.util.Arrays;

/**
 * 插入排序
 *  有序，无序之间的逻辑
 *  在有序列表中 拿到，去无序列表判定，进行排序即可
 */
public class InsertSort {

    public static void main(String[] args) {
        int[] arr ={8,7,5,99,88};
        sortFinalArr(arr);
    }


    public static void sortFinalArr(int ...arr){
        int listVal = 0;
        int indexVal = 0;
        for (int i = 1; i < arr.length; i++) {
            listVal = arr[i];
            indexVal = i - 1;
            while (indexVal >=0 && listVal < arr[indexVal]){
                arr[indexVal + 1] = arr[indexVal];// arr[insertIndex]
                indexVal--;
            }
           if (indexVal != i)
            arr[indexVal + 1] = listVal;
        }

        System.out.println("排序后");
        System.out.println(Arrays.toString(arr));


    }

    public static void sortArr(int ...arr){
        /**
         * 进行分析
         * 8,7,5,99,88
         * 有序  为 7 ， 无序为8，7,5,99,88
         */

        int listVal = arr[1];
        int indexVal = 0;

        while (indexVal >=0 && listVal < arr[indexVal]){
            arr[indexVal + 1] = arr[indexVal];// arr[insertIndex]
            indexVal--;
        }

        arr[indexVal + 1] = listVal;


        System.out.println("排序后");
        System.out.println(Arrays.toString(arr));

    }
}
