package com.fw.structure.linked;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Objects;
import java.util.concurrent.atomic.LongAdder;

/**
 * @Author:yanwei
 * @Date: 2020/12/5 - 23:37
 *
 * 单向链表得结构 —— 带 head 上得
 */

public class LinkedListDemo {

    public static void main(String[] args) {
        LinkedListThis linkedListThis = new LinkedListThis();
        linkedListThis.add("张三");
        linkedListThis.add("李四");
        linkedListThis.add("王五");
        linkedListThis.lists();
    }

}

// 单向列表具体实现
class LinkedListThis{
    // 先声明一个头部节点
    private NodeChild<Void> head = new NodeChild<>(-1,null,null);
    private LongAdder longAdder = new LongAdder();

    public void add(String val){
        NodeChild<String> nodeChild = new NodeChild<>(longAdder.intValue(),val,null);
        // 判断该头部下元素值
        if (Objects.isNull(head.getNodeChild())){
            head.setNodeChild(nodeChild);
        }else{
            NodeChild   temp = head.getNodeChild();
            while (Objects.nonNull(temp)){

                    if (Objects.isNull(temp.getNodeChild())){
                        // 第一次添加
                        temp.setNodeChild(nodeChild);
                        break;
                    }

                    temp = temp.getNodeChild();
            }
        }
        longAdder.increment();
    }


    // 遍历方法
    public void lists(){
        NodeChild temp = head.getNodeChild();
        if (Objects.nonNull(temp)){
            while (Objects.nonNull(temp)){
                System.out.println(temp);
                temp = temp.getNodeChild();
            }

        }

    }




    // 节点值
    @Data
    @AllArgsConstructor
    @ToString(exclude = "nodeChild")
    class  NodeChild<V>{
        private int index; // 属性值
        private V val;   // 属性值
        private NodeChild nodeChild; // 链表节点
    }

}


