package com.fw.structure.linked;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Objects;

/**
 * @Author:yanwei
 * @Date: 2020/12/5 - 23:37
 *
 * 单向链表得结构 —— 带 head 上得  根据编号插入
 */

public class LinkedListHostDemo {

    public static void main(String[] args) {
        LinkedListHostThis linkedListThis = new LinkedListHostThis();
        linkedListThis.add(12,"李四");
        linkedListThis.add(10,"张三");
        linkedListThis.add(5,"李四");
        linkedListThis.add(1,"李四");
        linkedListThis.add(7,"王五");
        linkedListThis.lists();
        linkedListThis.update(1,"嘿嘿");
        linkedListThis.update(12,"嘿嘿");
        linkedListThis.lists();
        linkedListThis.del(1);
        linkedListThis.del(12);
        linkedListThis.lists();
    }

}

// 单向列表具体实现
class LinkedListHostThis{
    // 先声明一个头部节点
    private NodeHostChild<Void> head = new NodeHostChild<>(-1,null,null);
    // 添加方法
    public void add(int index,String val){
        NodeHostChild<String> nodeChild = new NodeHostChild<>(index,val,null);
        // 判断该头部下元素值
        if (Objects.isNull(head.getNodeChild())){
            head.setNodeChild(nodeChild);
        }else{
            NodeHostChild   temp = head;
            while (Objects.nonNull(temp)){
                if (temp.getNodeChild() == null) break;
                // 根据编号考虑
                int tempIndex = temp.getNodeChild().getIndex();
                if (tempIndex == index) throw new IndexOutOfBoundsException("编号重复！");
                if (tempIndex  > index)
                    break;
                    temp = temp.getNodeChild();
            }
            nodeChild.setNodeChild(temp.getNodeChild());
            temp.setNodeChild(nodeChild);

        }
    }

    /**
     * 进行更新操作
     * @param index
     * @param val
     */
    public void update(int index,String val){
        NodeHostChild<Void> temp = this.head;
        boolean flag = false; // 默认 false
        while (Objects.nonNull(temp.getNodeChild())){
            if (temp.getNodeChild().getIndex() == index) {
                flag = true;
                break;
            }
            temp = temp.getNodeChild();
        }
        if (flag){
            temp.getNodeChild().setVal(val);
        }
    }

    /**
     * 进行删除操作
     */
    public void del(int index){
        NodeHostChild<Void> temp = this.head;
        boolean flag = false; // 默认 false
        while (Objects.nonNull(temp.getNodeChild())){
            if (temp.getNodeChild().getIndex() == index) {
                flag = true;
                break;
            }
            temp = temp.getNodeChild();
        }
        if (flag){
            temp.setNodeChild(temp.getNodeChild().getNodeChild());
        }

    }


    // 遍历方法
    public void lists(){
        NodeHostChild temp = head.getNodeChild();
        if (Objects.nonNull(temp)){
            while (Objects.nonNull(temp)){
                System.out.println(temp);
                temp = temp.getNodeChild();
            }

        }

    }




    // 节点值
    @Data
    @AllArgsConstructor
    @ToString(exclude = "nodeChild")
    class  NodeHostChild<V>{
        private int index; // 属性值
        private V val;   // 属性值
        private NodeHostChild nodeChild; // 链表节点
    }

}


