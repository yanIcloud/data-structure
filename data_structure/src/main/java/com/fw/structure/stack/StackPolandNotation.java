package com.fw.structure.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StackPolandNotation {

    public static void main(String[] args) {

        //先定义给逆波兰表达式
//(30+4)×5-6 =>304+5 × 6-=>164
// 4 * 5 - 8 + 60 + 8 / 2 => 4 5 * 8 - 60 + 8 2 / + //测试
//说明为了方便，逆波兰表达式 的数字和符号使用空格隔开 //String suffixExpression = "30 4 + 5 * 6 -";
        String suffixExpression = "4 5 * 8 - 60 + 8 2 / +"; // 76
//思路
//1. 先将 "34+5 × 6-"=> 放到ArrayList中
//2. 将 ArrayList 传递给一个方法，遍历 ArrayList 配合栈 完成计算
        List<String> list = getListString(suffixExpression);
        System.out.println("rpnList=" + list);
        int res = calculate(list);
        System.out.println("计算的结果是=" + res);
        // 表达式转换 (3+4)×5-6
        List<String> toInfixExpression = toInfixExpression("(3+4)*5-6");
        List<String> parseSuffixExpression = parseSuffixExpression(toInfixExpression);
        System.out.println(calculate(parseSuffixExpression));
    }

    //将一个逆波兰表达式， 依次将数据和运算符 放入到 ArrayList 中 //将 suffixExpression 分割
    public static List<String> getListString(String suffixExpression) {
        String[] split = suffixExpression.split(" ");
        List<String> list = new ArrayList<String>();
        for (String ele : split) {
            list.add(ele);
        }
        return list;
    }



    /**
     * 把字符串转换成中序表达式 : 比如 2+(3-4), 并放入到List中
     * @param
     * @return
     */
    public static List<String> toInfixExpression(String infixExpression) {
        List<String> ls = new ArrayList<String>();//存储中序表达式
        int i = 0;
        String str;
        char c;
        do {
            //如果c 在 < 48 或者 > 57 说明是符号, 这里没有判断是 + , - , * , / 等等
            if ((c = infixExpression.charAt(i)) < 48 || (c = infixExpression.charAt(i)) > 57) {
                ls.add("" + c);
                i++;
            } else { // 说明是数字，要进行拼接处理
                str = "";
                while (i < infixExpression.length() && (c = infixExpression.charAt(i)) >= 48
                        && (c = infixExpression.charAt(i)) <= 57) {
                    str += c;
                    i++;
                }
                ls.add(str);
            }

        } while (i < infixExpression.length());
        return ls;
    }

    /**
     * 将一个中缀表达式对应的List 转成 转换成逆波兰表达式, 放入到List中
     * @param ls
     * @return
     */
    public static   List<String> parseSuffixExpression(List<String> ls) {
        Stack<String> s1=new Stack<String>();
        Stack<String> s2=new Stack<String>();
        List<String> lss = new ArrayList<String>();
        for (String ss : ls) {
            if (ss.matches("\\d+")) {
                lss.add(ss);
            } else if (ss.equals("(")) {
                s1.push(ss);
            } else if (ss.equals(")")) {

                while (!s1.peek().equals("(")) {
                    lss.add(s1.pop());
                }
                s1.pop();
            } else {
                while (s1.size() != 0 && Operation.getValue(s1.peek()) >= Operation.getValue(ss)) {
                    lss.add(s1.pop());
                }
                s1.push(ss);
            }
        }
        while (s1.size() != 0) {
            lss.add(s1.pop());
        }
        return lss;
    }



    //完成对逆波兰表达式的运算
	/*
* 1)从左至右扫描，将 3 和 4 压入堆栈;
2)遇到+运算符，因此弹出 4 和 3(4 为栈顶元素，3 为次顶元素)，计算出 3+4 的值，得 7，再将 7 入栈; 3)将 5 入栈;
4)接下来是×运算符，因此弹出 5 和 7，计算出 7×5=35，将 35 入栈;
5)将 6 入栈;
6)最后是-运算符，计算出 35-6 的值，即 29，由此得出最终结果
*/
    public static int calculate(List<String> ls) {
// 创建给栈, 只需要一个栈即可
        Stack<String> stack = new Stack<String>();
// 遍历 ls
        for (String item : ls) {
// 这里使用正则表达式来取出数
            if (item.matches("\\d+")) { // 匹配的是多位数
// 入栈
                stack.push(item);
            } else {
// pop 出两个数，并运算， 再入栈
                int num2 = Integer.parseInt(stack.pop());
                int num1 = Integer.parseInt(stack.pop());
                int res = 0;
                if (item.equals("+")) {
                    res = num1 + num2;
                } else if (item.equals("-")) {
                    res = num1 - num2;
                } else if (item.equals("*")) {
                    res = num1 * num2;
                } else if (item.equals("/")) {
                    res = num1 / num2;
                } else {
                    throw new RuntimeException("运算符有误");
                }
//把 res 入栈
                stack.push("" + res);
            }
        }
//最后留在 stack 中的数据是运算结果
        return Integer.parseInt(stack.pop());
    }

}

class Operation {
    private static int ADDITION=1;
    private static int SUBTRACTION=1;
    private static int MULTIPLICATION=2;
    private static int DIVISION=2;

    public static int getValue(String operation){
        int result;
        switch (operation){
            case "+":
                result=ADDITION;
                break;
            case "-":
                result=SUBTRACTION;
                break;
            case "*":
                result=MULTIPLICATION;
                break;
            case "/":
                result=DIVISION;
                break;
            default:
//                System.out.println("不存在该运算符");
                result=0;
        }
        return result;
    }
}
