package com.fw.structure.stack;

/**
 * @Author:yanwei
 * @Date: 2021/1/17 - 18:53
 *
 *  访栈结构，根据数组进行设定
 */

public class ArrayStack {
    public static void main(String[] args) {
         IntArrayStack intArrayStack = new IntArrayStack(5);
         intArrayStack.pushItem(1);
        intArrayStack.pushItem(2);
        intArrayStack.pushItem(3);
        intArrayStack.pushItem(4);
        intArrayStack.pushItem(5);
        intArrayStack.pushItem(6);

        intArrayStack.listStack();
        System.out.println(intArrayStack.pop());
        System.out.println(intArrayStack.pop());
        System.out.println(intArrayStack.pop());
        System.out.println(intArrayStack.pop());
        System.out.println(intArrayStack.pop());
        System.out.println(intArrayStack.pop());


    }
}

class IntArrayStack{
    private int top = 0; // 栈顶
    private int size; // 总数量
    private int[] itemStack;  // 栈元素
    public IntArrayStack(int size){
        this.size = size;
        this.itemStack = new int[size];
    }

    // 栈是否满了
    public boolean isFull(){

        return top == size;
    }

    // 栈是否为空
    public boolean isNull(){
        return top == 0;
    }

    /**
     * pop 取出栈元素
     */
    public int pop(){
        if (isNull()) return -1;
        top --;
        int value = itemStack[top];
        return value;
    }


    /**
     * 存储栈元素
     */
    public void  pushItem(int item){
        if (isFull()) return;
        itemStack[top] = item;
        top ++;

    }

    /**
     * 遍历栈元素
     */
    public void listStack(){

        for (int i = top - 1; i >= 0; i--) {
            System.out.printf("当前索引[%d]:[%d] \n",i,itemStack[i]);
        }

    }



}
