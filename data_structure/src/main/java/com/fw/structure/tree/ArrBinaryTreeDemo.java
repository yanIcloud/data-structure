package com.fw.structure.tree;

/**
 * 顺序二叉树
 * 1.  顺序二叉树通常只考虑完全二叉树
 * 2.  第n个元素的左子节点为 2 * n + 1
 * 3.  第n个元素的右子节点为 2 * n + 2
 * 4.  第 n 个元素的父节点为 (n-1) / 2
 * 5.  n : 表示二叉树中的第几个元素
 */
public class ArrBinaryTreeDemo {


    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8};

        ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
        arrBinaryTree.perArrIndex(0);
    }
}

/**
 * 顺序二叉树的实例
 * 其实对应的 n 就是 数组下标
 */
class ArrBinaryTree{
    private int[] arr;

    public ArrBinaryTree(int[] arr) {
        this.arr = arr;
    }

    /**
     *  前序遍历
     * @param index 就是 n
     */
    public void perArrIndex(int index){
        if (arr == null || arr.length == 0 || arr.length < index){
            System.out.println("有问题!");
        }else{
            // 前序遍历
            System.out.println(arr[index]);
            // 左子节点
            if (2 * index + 1 < arr.length)
            perArrIndex(2 * index + 1);
            // 右子节点
            if (2 * index + 2 < arr.length)
                perArrIndex(2 * index + 2);
        }
    }
}