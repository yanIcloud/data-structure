package com.fw.structure.hashtable;

import java.util.Scanner;

/**
 * 自定义实现 哈希表 方式  通过 数组 + 链表方式进行实现
 */
public class HashTableMain {

    public static void main(String[] args) {

//创建哈希表
        HashLinkArr hashTab = new HashLinkArr(7);
//写一个简单的菜单
        String key = "";
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("add: 添加雇员");
            System.out.println("list: 显示雇员");
            System.out.println("find: 查找雇员");
            System.out.println("exit: 退出系统");
            key = scanner.next();
            switch (key) {
                case "add":
                    System.out.println("输入 id");
                    int id = scanner.nextInt();
                    System.out.println("输入名字");
                    String name = scanner.next(); //创建 雇员
                    Emp emp = new Emp(id, name);
                    hashTab.add(emp);
                    break;
                case "list":
                    hashTab.list();
                    break;
                case "del":
                    System.out.println("输入 id");
                     id = scanner.nextInt();
                     hashTab.del(id);
                    break;
                case "find":
                    System.out.println("请输入要查找的 id");
                    id = scanner.nextInt();
                    Emp tabById = hashTab.findById(id);
                    System.out.println(tabById == null ? null : tabById.getId() + tabById.getName());
                    break;
                case "exit":
                    scanner.close();
                    System.exit(0);
                default:
                    break;
            }
        }
    }


/**
 * 哈希表结构  数组 + 链表
 */

static class HashLinkArr{
    private EmpLinkList[] empLinkLists;
    private int size;

    public HashLinkArr(int size) {
        this.size = size;
        // 初始化
        empLinkLists = new EmpLinkList[size];
        for (int i = 0; i < empLinkLists.length; i++) {
            empLinkLists[i] = new EmpLinkList();
        }
    }

    /**
     * 添加
     */
    public void add(Emp e){
        empLinkLists[hashFull(e.getId())].add(e);
    }
    /**
     * 查找
     */
    public Emp findById(int id){
         return empLinkLists[hashFull(id)].findById(id);
    }

    /**
     * 遍历
     */
    public void list(){
        for (int i = 0; i < empLinkLists.length; i++) {
            empLinkLists[i].list();
        }
    }

    /**
     * 删除
     */
    public void del(int id){
           empLinkLists[hashFull(id)].del(id);
    }


    /**
     * 取模的方式 算出存放位置
     */
    private int hashFull(int id){
        return id % size;
    }
}

/**
 * 链表
 */
static class EmpLinkList {
    private Emp head = null;

    /**
     * 增加方法
     */
    public void add(Emp e) {
        if (head == null) {
            head = e;
            return;
        }
        Emp temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = e;
    }

    /**
     * 遍历方法
     */
    public void list() {
        if (head == null) return;
        Emp temp = head;
        while (true) {
            System.out.printf("id=%s,name=%s\t", temp.getId(), temp.getName());
            if (temp.next == null) {
                break;
            }
            temp = temp.next;
        }
    }

    /**
     * 查找方法
     */
    public Emp findById(int empId) {
        Emp temp = head;
        while (temp != null) {
            if (temp.getId() == empId) {
                return temp;
            }
            temp = temp.next;
        }
        return null;
    }

    /**
     * 删除方法
     */
    public void del(int empId){
        Emp temp = head;
        // 如果头部第一就是
        if (temp != null && temp.getId() == empId){
            head = temp.next;
        }
        boolean flag = false;
        while (temp.next != null) {
            // 保留当前

            if (temp.next.getId() == empId) {
                // temp 需要剔除
                flag = true;
                break;
            }
            temp = temp.next;
        }
        // 剔除
        if (flag){
            temp.next = temp.next.next;
        }


    }
}


/**
 * 雇员表
 */
static class Emp {
    private int id;
    private String name;
    // 下一个指向指针
    protected Emp next;

    public Emp(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
}
